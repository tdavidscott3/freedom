# WHAT IS freedom #
The freedom framework is a framework with centralized decentralized programming in mind.

# How to help with developing #
Send me a request to set up your branch so you can start working. This is an open source framework, so yeah.

# submodules - git the code #
**THIS IS IMPORTANT**
In order to use submodules, you need to perform the following command
***git submodule add git@bitbucket.org:tdavidscott3/freedom.git library***
the above command will clone the freedom framework into a library directory



## freedom library directory structure.

>       /
>       +-- conf.ini (conf.ini.sample)
>       +-- conf.php -> ./system_classes/conf.php
>       +-- application_classes
>       +-- system_classes
>       |       +-- session_class.php
>       |       +-- system_class.php
>       |       +-- debug_class.php
>       |       +-- database_class.php
>       |       +-- mysql_class.php
>       |       +-- pgsql_class.php
>       |       +-- oracle_class.php
>       |       +-- access_class.php
>       |       +-- mongo_class.php
>       |       +-- couch_class.php
>       |       +-- common_class.php
>       |       +-- error_class.php
>       |       +-- nt_auth_class.php
>       |       +-- unix_auth_class.php
>       |       `-- shell_class.php
>       +3rd_party_classes
>       `-- index.php

# How to use this framework

This framework allows the flexibilty of using a conf.ini file which allows one change multiple affects. However this framework allows the deviation
from the framework to include only what is needed at the time it is needed. The only file that is needed to make this entire framework function
correctly is the session_class.php located inside the library/system_classes directory.

## Explainations

I'll attempt to do a break down of the most important parts of freedom.

+ session_class.php
+ database_class.php
    * mysql_class.php
    * pgsql_class.php
    * oracle_class.php
    * mongo_class.php
    * couch_class.php
    * access_class.php

### session_class.php
This class handles everything dealing with the session or $_SESSION variable. There are a few functions that are probably the most important above all other functions.
__Include the session_class file and create the object.__
> require_once($_SERVER['DOCUMENT_ROOT'].'/library/system_classes/session_class.php');

> $session = new session_class();


* set($key, $value) - set a key to a value
    * Set a key to a passed in value.
        * the value can be a string, array, or an object.
    * > $session->set('user_key',array('name'=>'jim boe','email'=>'e@mail.com'));
    
* get($key) - retrieve the value of a key
    * Retrieve a value located inside the key of $_SESSION
    * > $session->get('key');


* loadFile($fileName, $FILE, $LINE) - load or require_once a file
    * require_once a file
        + $FILE is the fileName of the file calling the loadFile method.
        + $LINE the line number of the file calling the loadFile method.
    * > $session->loadFile('filename',\_\_LINE\_\_,\_\_LINE\_\_);


* loadClass$fileName, $className, $FILE, $LINE); - create an object after require_once of a class file.
    + require_once, instantiate the class, and return the object
        + $FILE is the fileName of the file calling the loadClass method
        + $LINE is the line number of the file from where the loadClass method is called.
    + > $session->loadClass('/file/class_name.php','class_name',\_\_FILE\_\_,\_\_LINE\_\_);



/**
                        DATABASE_CLASS and database freedom.
/**
*        The database_class.php file is a wrapper used so that the definition inside the conf.php class can be used without having to build each connection to the database.
*        although this is very useful, it is on some occasions that you will need to go beyond this file and include the correct dbname_class.php file directly.
*/

1. default conf.php connections
    * > $db = $session->loadClass(\_\_SYSTEM\_\_.'/database_class.php','database_class',\_\_FILE\_\_,\_\_LINE\_\_)
    * > $query_result = $db->fetch("SELECT * FROM table WHERE column = 'value'");
    * > print_r($query_result);

2. default conf.php connection w/ another connection to a seperate server
    * > $db = $session->loadClass(\_\_SYSTEM\_\_.'/database_class.php','database_class',\_\_FILE\_\_,\_\_LINE\_\_)
    * > $query_result = $db->fetch("SELECT * FROM table WHERE column = 'value'");
    * > print_r($query_result);

    * > $session->loadFile(\_\_SYSTEM\_\_.'/mysql_class.php',\_\_FILE\_\_,\_\_LINE\_\_);
    * > $db2 = new mysql_class('some_host','user','password')
    * > $query_result2 = $db2->fetch("SELECT * FROM table WHERE column = 'value'");
    * > print_r($query_result2);

3. Multiple DB connections NOT using the database class.
    * > $session->loadFile(\_\_SYSTEM\_\_.'/mysql_class.php',\_\_FILE\_\_,\_\_LINE\_\_)
    * > $db1 = new mysql_class('host_1','user','password')
    * > $db2 = new mysql_class('host_2','user','password')
    * > $db3 = new mysql_class('host_3','user','password')




==============
### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Legal ###
This is copyright and copyleft = copylight. Use in proprietary softwaer NEVER to be added to this repo. Open source projects only certain aspect useful to the core structure and operations will be added. - David Scott
