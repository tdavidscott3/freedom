<?PHP
/**
*	@author Josh Chick <joshchick@gmail.com>
*	@version 1.0.0
*	curl_class.php - the handling of anything with pulling pages using curl?
*/

//
// Description: Curl URLs to get back the data. You can pass extra 
//     options and customize the URL as needed.
//

/**
*	curl{}
*	@todo Please adjust the class definition to use the curl_class naming convention
*	This really needs to be documented by the author.
*/
class curl {

  public $curl;
  public $curl_opt;
  public $curl_url;
  public $url;
 
  public function __construct($base) {
    $this->base = $base;
  }

  // Setting the curl options 
  public function curl_opt() {
    curl_setopt($this->, CURLOPT_URL, $this->curl_url);
    curl_setopt($this->, CURLOPT_RETURNTRANSFER, 1);

    if(count($this->curl_opt) > 0) {
      foreach($this->curl_opt as $option => $value) {
        curl_setopt($this->curl, $option, $value);
      }
    }
  } // end curl_opt

  // Send a curl request to hipchat and return the json
  public function curl_url() {
    $this->curl_url = $this->base . $this->url;
    if(count($this->extra) > 0)
      $this->curl_url .= "&" . http_build_query($this->extra);
    $this->curl = curl_init();
    //  Sets the CURL options
    $this->curl_opt();
    return json_decode(curl_exec($this->curl), true);
  } // end curl_url
}
?>
