<?php
/**
*	php_dependencies.php
*	This file will check for certain dependencies that are requireed for the operations of this applications.
*	@author Craig Tierney <C.Tierney@netpass.com>
*/


/**
*	checkApplicationDependencies() - main function; Call this function which then executes the following checks.
*	@param array $exclude - set to exclude extensions, this passes through to the checkExtensions()
*	- canWeHandleClasses()
*	- checkExtensions()
*	@author Craig Tierney <C.Tierney@netpass.com>
*/
function checkApplicationDependencies($exclude = array()){
	ini_set('display_errors',false);
	/*
		We check to verify that this server has all the dependencies that we
		need for our application to run.
	*/
	// are we running php5 or less?
	canWeHandleClasses();
	
	
	// we exclude KNOWN extension we will not be using.
	//$exclude_ext = array('pgsql');
	// check for the extensions that we need for our application
	checkExtensions($exclude);
	
	
	ini_set('display_errors',true);
}

/**
*	canWeHandleClasses() - Due to a limitation in PHP4 where classes are not allowed, this function checks to verify that the application, or better yet that the PHP engine can handle classes and objects.
*	@param string $reason default null - passed in reason we actually failed.
*	@author Craig Tierney <C.Tierney@netpass.com>
*/
function failed($reason = null){
	?>
	<h1>Missing Dependency</h1>
	<p>Unfortainitly we are unable to execute this application. Please see the error below.</p>
	<p><pre><?= $reason; ?></pre></p>
	<?php
	exit();
}

/**
*	canWeHandleClasses() - Due to a limitation in PHP4 where classes are not allowed, this function checks to verify that the application, or better yet that the PHP engine can handle classes and objects.
*	@author Craig Tierney <C.Tierney@netpass.com>
*/
function canWeHandleClasses(){
	if(!defined('PHP_VERSION_ID')){
		define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
	}
	if(PHP_VERSION_ID < 50000){
	//$VER = explode('.',phpversion());
	//settype($VER[0],'integer');
	//if($VER[0] < '5') {
		failed("You are running php version ".phpversion()." please upgrade to Version 5 or higher to execute this application");
	}
}


/**
*	canWeHandleTraits() - traits are like classes except you can have a class use a trait. Anything less then version 5.4.0 will not be able to use traits.
*	@author David Scott <tdavid.scott3@gmail.com>
*/
function canWeHandleTraits(){
	setPhpVersion();
	if(PHP_VERSION_ID < 50400){
		failed("You are running php version ".phpversion()." please upgrade to Version 5 or higher to execute this application");
	}

}
function setPhpVersion(){
	if(!defined('PHP_VERSION_ID')){
		define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
	}
}

/**
*	checkExtensions() - This function will check for all the extensions that are needed for this application to operate. If a given extension is not loaded, or does not exist, then the failed() function is called, and a message is displayed.
*	@param array $exclude - we can state which extension to NOT to check for.
*	@author Craig Tierney <C.Tierney@netpass.com>
*/
function checkExtensions($exclude = array()){
	$ext = array(
		'gd',
		'mysql',
		'pgsql',
	);
	foreach($ext as $k => $v){
		foreach($exclude as $ek => $ev){
			if($ev != $v){
				$ext_load = @extension_loaded($v);
				$dyn_ext = @dl("$v.so");
				if(!$ext_load && !$dyn_ext) { failed("Missing extension $v"); }
			}
		}
	}
}

?>
