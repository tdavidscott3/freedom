<?php
/**
*	auth_class.php
*	aUTHORIZATION CLASS
*/

/**
*	auth_class{}
*/
class auth_class{

	private $auth_type = null;
	private $ldap = null;
	private $unix = null;

	public function __construct($conf = null){
		if($conf == null){
			$this->conf = $_SERVER['DOCUMENT_ROOT'].'/library/conf.php';
		}else{
			$this->conf = $conf;
		}
		require_once(__SYSTEM__'./authorization_classes/authorization_class.php');
		$this->nt = new nt_auth_class();

		require_once(__SYSTEM__.'/authorization_classes/unix_auth.php');
		$this->unix = new unix_auth_class();
	}

	public function set($type){
		$this->auth_type = $type;
	}

	public function get($str){
		switch($str){
			case 'type' : return $this->auth_type; break;
		}
	}

	public function bind($username, $password){
		switch($this->auth_type){
			case 'unix' :
				return $this->unix->bind($username,$password);
			break;
			case 'nt':
			case 'ldap' :
				return $this->ldap->bind($username, $password);
			break;
			default :
				return array(0=>'error',1=>"Unknown authorizxation type '".$this->auth_type."'";
			break;
		}
	}

}
