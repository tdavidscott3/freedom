<?php
/**
*	nt_auth_class.php - all nt side of things.
*	@version 1.0.1
*	@author David Scott <tdavid.scott3gmail.com
*	@author Saad Anwar
*
*/



/**
*	nt_auth_class{}
*/
class nt_auth_class{

	/**
	*	@var string $host - the server we are connecting to for NT ldap things.
	*/
	private $host = '';

	/**
	*	@var string $username - the username used to connect to the server
	*/
	private $user = '';

	/**
	*	@var string $password - the password to use with the username to connect to $this->host
	*/
	private $pass = '';

	/**
	*	@var string $login - this is built, and is usually $this->domain\$this->user
	*/
	private $login = '';	// this can change	($this->domain\$this->user)

	/**
	*	@var string $groupName default '*DOC (TBS)'
	*/
	private $groupName = '';	// the group that is OK to log into the system.

	public $filter = '';
	public $dn = "dc=domain, dc=tld";
	/**
	*	@var object $myInfo - the assigned object of the user logging into the system
	*/
	public $myInfo = null;	// where we are storing the information gotten back from NT

	public function __construct($host = null,$user = null, $pass = null){
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass
	}


	/**
	*	ldapSetOptions() - Set the LDAP options for interactions
	*	@return boolean always returns true
	*/
	public function ldapSetOptions(){
		ldap_set_option($this->ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($this->ldap, LDAP_OPT_REFERRALS, 0);
		return true;
	}

	/**
	*	authorize() - the function used to authenticate the user when attempting to login
	*	@param $username the username supplied
	*	@param $password of the username
	*	@return array (0 => 'error', 1 => 'Error Message') -OR- (0 => 'success')
	*/
	public function authorize($username, $password){
		// we are autorizing this user to be able to use this system... we get our details from AD
		// and put them in our DB, for speed.
		ldap_connect($this->host);
		$this->ldapSetOptions();

		if(ldap_bind($this->ldap, $this->domain."\\$username", $password)){
			return array(0=>'success');
		}

		ldap_close($this->ldap);	// close our connection!

		return array(0=>'error',1=>'Authorization Failed');
	}

	/**
	*	getEmailAddress()
	*	@param $user
	*	@return string
	*	This will return the email address assigned to the myInfo public variable.
	*/
	public function getEmailAddress($user){
		return $this->myInfo[0]['userprincipalname'][0];	// this IS the email addressw
	}

	/**
	*	search()
	*	@param $username
	*	@return array (0 => 'error', 1=> 'group located') <b>-OR-</b> (0 => 'error', 1 => 'invalid group') <b>-OR-</b> (0 => 'success')
	*/
	public function search($username){
		// first we search for this user in AD
		$this->ldap = ldap_connect($this->host) or exit('LDAP Connection failure');
		$this->ldapSetOptions();
		$this->bind = ldap_bind($this->ldap,$this->domain."\\".$this->user, $this->pass) or exit("LDAP Bind failure");
<<<<<<< HEAD:library/system_classes/authorization_classes/nt_auth_class.php
=======
		$this->dn = "dc=".$this->dc[1].", dc=".$this->dc[1];
		$this->filter = "(".$this->filter."=$username)";
>>>>>>> master:library/system_classes/nt_auth_class.php
		$this->sr = ldap_search($this->ldap, $this->dn, $this->filter);
		$this->myInfo = ldap_get_entries($this->ldap, $this->sr);
		
		// this doesn't work, as the value is longer then what we need to search for
		//if(in_array($this->groupName, $info[0]['memberof'])) { return true; }
		if($this->myInfo['count'] == 0){
			$returnArray = array(0=>'error', 1=>"Unable to find username '$username'");
		}
		foreach($this->myInfo[0]['memberof'] as $k => $v){
			$splits = explode(',',$v);
			if($splits[0] == 'CN=*DOC (TBS)'){
				$returnArray = array(0=>'success',1=>'group located');
			}
		}

		if(!isset($returnArray)){
			return array(0=>'error',1=>'invalid group');
		}else{
			return $returnArray;
		}
	}

	/**
	*	searchRet()
	*	@param $user default is null
	*	@return object 
	*/
	public function searchRet($user = null){
		// first we search for this user in AD
		$this->ldap = ldap_connect($this->host) or exit('LDAP Connection failure');
		$this->ldapSetOptions();
		$this->bind = ldap_bind($this->ldap, $this->domain."\\".$this->user, $this->pass) or exit("LDAP Bind failure");
		$this->sr = ldap_search($this->ldap, $this->dn, $this->filter) or exit('search failure');
		$this->myInfo = ldap_get_entries($this->ldap, $this->sr);
		ldap_close($this->ldap);
		return $this->myInfo;
	}

	/**
	*	getDisplayName()
	*	@param $user default is <b>null</b>
	*	@return string $this->myInfo[0][displayname'][0]
	*/
	public function getDisplayName($user = null){
		$tmp = $this->search($user);	// due to an issue, we-research for the user to return the displayName
		if(!isset($this->myInfo[0]['displayname'][0])){
			print("<h4 style=\"color: red;\">Display Name Failure</h4><pre>");
			print_r($this->myInfo);
			exit('Unable to get your Display Name, exiting....');
		}
		return $this->myInfo[0]['displayname'][0];

	}
	public function setGroup($groupname = null){
		$this->groupName = $goupName;
	}
	public function setFilter($filter = null){
		$this->filter=$filter;
	}
	public function setDC($dc){
		$this->dc = $dc;
	}
	public function setDN($DN){
		$this->DN = $DN;
	}

}

?>
