<?php
/**
*	unix_auth_class.php - all nt side of things.
*	@version 1.0.1
<<<<<<< HEAD:library/system_classes/authorization_classes/unix_auth_class.php
*	@author David Scott <TDavid.Scott3@gmail.com>
=======
*	@author David Scott <tdavid.scott3@gmail.com>
*	@author Saad Anwar
>>>>>>> master:library/system_classes/unix_auth_class.php
*	@todo Fix the entire class, as the variables are only declare upon __construct() -- DO IT AGAIN
*
*/


/**
*	unix_auth_class{}
*/
class unix_auth_class{

  private $dn = null;
  private $dc = null;
  private $test = null;
  private $personfilter = null;
  private $filter = null;

  public function __construct($host = null, $uname = null, $pass = null) {
    // Class Variables
    $this->host = $host;
    $this->setUname($uname);;
    $this->pass = $pass;
    $this->myInfo = null; 
    $this->ldap = null;
    $this->test = "";
  }

  public function setDN($DN){ $this->DN = $DN; }
  public function setDC($DC){ $this->dc = $DC; }
  public function setFilter($filter){ $this->filter = $filter; }
  public function setPersonFilter($filter){ $this->personfilter = $filter; }
  public function setUname($uname){ $this->uname = $uname; }


  // Look for common requet in LDAP 
  public function commonRequests($req) {
    // Local Variables
    $res = "";
    // Pull requested data
    switch($req) {
      case "mail":
      case "email": $res = $this->myInfo[0]["mail"][0]; break;
      case "uidnumber":
      case "uid": $res = $this->myInfo[0]["uidnumber"][0]; break;
      case "gidnumber":
      case "gid": $res = $this->myInfo[0]["gidnumber"][0]; break;
      case "password": $res = $this->myInfo[0]["sasloginconfiguration"][0]; break;
      case "membersof":
      case "groups": $res = $this->myInfo[0]["groupmembership"][0]; break;
      case "keys":
      case "sshkeys": $res = $this->myInfo[0]["dmtsshpublickey"][0]; break;
    }
    return $res;
  } // END commonRequests

  // Custom LDAP Options
  public function ldapSetOptions(){
    ldap_set_option($this->ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($this->ldap, LDAP_OPT_REFERRALS, 0);
  } // END ldapSetOptions

  // Creates the connection and binds the resource to ldap
  public function connect() {
    $this->ldap = ldap_connect($this->host);
    $this->ldapSetOptions();
    if(ldap_bind($this->ldap, $this->uname, $this->pass)) {
      // Successful Bound LDAP
      return array(0 =>'success');
    }
    else
      // Failed to Bound LDAP
      return array(0=>'error',1=>'Authorization Failed');
  } // END connect

  // Closes the ldap resource
  public function close() {
    ldap_close($this->ldap);
  } // END close

  // Test login for user and pass
  public function authorize($username, $password){
    // Connect to LDAP server
    ldap_connect($this->host);
    $this->ldapSetOptions();

    // Test the user and password bind	
    if(ldap_bind($this->ldap, $username, $password)){
      $this->close($this->ldap);
      return array(0=>'success');
    }
    return array(0=>'error',1=>'Authorization Failed');
  } // END authorize

  // Gets all the user data
  public function getRawData($username) {
    $connected = $this->connect();
    $search_res = "cn=" . $username . "," . $this->dn;
    $this->myInfo = ldap_get_entries($this->ldap, ldap_search($this->ldap, $search_res, $this->filter));
    return $this->myInfo;
  } // END getRawData

  // Search for specific requested items
  public function search($username, $searchRequest="all"){
    // local Variable
    $resArray = array();

    // Get all the LDAP Raw Data	
    $this->getRawData($username);

    // Sanity Check -- convert String to Array
    if(is_string($searchRequest))
      $searchRequest = array(0 => $searchRequest);

    // Sanity Check -- Dump Data for all
    if(in_array("all", $searchRequest))
      return $this->myInfo;

    // Search and return requested info	 
    foreach($searchRequest as $req) {
      $results = $this->commonRequests($req);
      array_push($resArray, $results);
    }
    return $resArray;
  } // END search

} // END class

?>


