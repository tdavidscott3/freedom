<?php
/**
*	common_class.php - all common functions, put inside a class....
*	@author David Scott <tdavid.scott3@gmail.com>
*/



/**
*	common_class{}
*/
class common_class{
	/**
	*	@var object $debug default is null
	*/
	private $debug = null;
	
	/**
	*	__construct()
	*	@return object $this->debug
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function __construct(){
		require_once('conf.php');
		require_once(__SYSTEM__.'/debug_class.php');
		$this->debug = new debug_class();
	}
	
	/**
	*	isNull() - checks to see if the variable passed in is null or 'null'
	*	@param variable $var default is null
	*	@return boolean true|false
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function isNull($var = null){
		if($var == null || $var == 'null'){
			return true;
		}
		return false;
	}
	
	/**
	*	isEmpty() - returns true or false based on if the variable passed in is empty.
	*	@param variable $var default is null
	*	@return boolean true|false
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function isEmpty($var = null){
		if($var == '' || empty($var)){
			return true;
		}
		return false;
	}
	
	/**
	*	debug() - this method will called the display method from the debug object declared on instanciation.
	*	This will also, expect that the variable passed in, is a string only. As this will also, perform some magic to
	*	show the values assigned to the variable aspect of the string... or <b>print_r($$s)</b>
	*	@param string $s default is null - something like '_SESSION', '_REQUEST', '_COOKIE'
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function debug($s = null){
		$this->debug->display($s,$$s);
	}
	
	/**
	*	randomGenerate() - generate a random string, used for creating passwords
	*	@param integer $min default 5
	*	@param integer $max default 10
	*	@return string $str
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function randomGenerate($min = 5, $max = 10){
		$len = mt_rand($min,$max);
		$str = '';
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0987654321`~!@#$%^&*()_+|-=\\[]{};:,<.>/?';
		// print("<br>\$count = ".strlen($chars)."<br>");
		for($i = 0; $i<$len+1;$i++){
			$n = mt_rand(0, strlen($chars)-1);
			// print("<br>\$n = $n<br>");
			$str = $str.$chars[$n];
		}
		
		return $str;

	}

	/**
	*	checkThis() - checks the existance of a variable and then the assignment of that variable - or something like that
	*	@param string $str default-less
	*	@param string $default default null
	*	@return string $str|$default
	*/
	public function checkThis($str, $default = null){
		if(!isset($str)){ return $default; }else{ return $str; }
	}

	/**
	*	amChecked() - this is used for checking to see what item is seletced... useful in drop downs, checkboxes, etc.
	*	@param string $str default-less
	*	@return string 'CHECKED'
	*/
	public function amChecked($str){
		if(isset($str) && $str = 'on'){ return "CHECKED"; }
	}

	/**
	*	limitChars() - return a limited length of the string passed in
	*	@param string $str default-less
	*	@param integer $count default 100
	*	@return string $retVal
	*/
	public function limitChars($str, $count=100){
		$retVal = '';
		for($i=0;$i<$count;$i++){
			$retVal .= $str[$i];
		}
		return $retVal;
	}

	/**
	*	makeHTML() - converts a string into an HTML &lt;br> delimented string
	*	@param string $string default null
	*	@return string
	*/
	public function makeHTML($string = null){
		$converterArray = array(
			"\n" => "<br>",
		);

		return strtr($string,$converterArray);
	}

	/**
	*	is_urlEncoded() - this will check to see if the passed in string is already url_encoded or not. Returns true||false
	*	@param string $str default null
	*	@return boolean
	*/
	public function is_urlEncoded($str = null){
		if($str == urldecode($str)){
			return false;
		}
		return true;
	}

	/**
	*	truncateString() - this is the same as the limitChars().
	*	@param integer $int default 250
	*	@param string $str default null
	*	@return string
	*/
	public function truncateString($int = 250, $str = null){
		return substr($str, 0, $int);
	}
	
	/**
	*	YesOrNo() - this function will return a string based on the true or falseness of the boolean passed in
	*	@param boolean $b
	*	@param string $true
	*	@param string $false
	*	@return string
	*/
	public function YesOrNo($b = false, $true = 'True', $false = 'False', $trueColor = 'green', $falseColor = 'red', $bold = 'true'){
		switch($bold){
			case 'true' : $bold = array('pre'=>'<b>','post'=>'</b>');break;
			case 'false' : $bold = array('pre'=>'','post'=>'');break;
		}

		switch($b){
			//case true: <- this is ALWAYS met!
			case 't':
			case 'T':
			case 'true' :
				$reTurn = "<span style=\"color: $trueColor;\">{$bold['pre']}$true{$bold['post']}</span>";
				print($reTurn);
			break;
			//case false: <- this will never be met
			case 'f':
			case 'F':
			case 'false':
				$reTurn = "<span style=\"color: $falseColor;\">{$bold['pre']}$false{$bold['post']}</span>";
				print($reTurn);
			break;
		}
		//print("<h1>\$b == $b</h1>");
		return $reTurn;
	}
}
?>
