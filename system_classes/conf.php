<?php
/**
*	This is our configurations file.
*	It is used to set default items based on the application.
*/

/**
*	We check to see if this File is executed directory, and it is... we STOP RIGHT THERE
*/
if(preg_match('/conf/i',$_SERVER['REQUEST_URI'])){
	die(header("Location: /"));
	exit();
	exit();
	die();
}

if(!defined('__HOSTNAME__')){ define('__HOSTNAME__',gethostname()); }
if(!defined('__DOMAIN__')){ define('__DOMAIN__',"some logic"); }

function processConfIniFile($section){
	foreach($section as $sectionKey => $sectionValue){
		if(!defined(strtoupper($sectionKey))) {
			//error_log("define(".strtoupper($sectionKey).",$sectionValue)");
			define(strtoupper($sectionKey),$sectionValue);
		}
	}
}
if(file_exists($_SERVER['DOCUMENT_ROOT'].'/conf.ini')){
	$ini = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/conf.ini',true);
}else{
	die("Missing conf.ini file from the ".$_SERVER['DOCUMENT_ROOT']." directory. Can not continue.<br>Please ensure that your conf.ini file is located in the ".$_SERVER['DOCUMENT_ROOT']." directory.");
}

processConfIniFile($ini['db']);
processConfIniFile($ini['system']);

// let's sut the DB name to use
$pattern ='/'. __PROD__.'/i';
if(preg_match($pattern,$_SERVER['SERVER_NAME'],$these_matches)){
        define('__DB_NAME__',__PROD_DB__);
}else{
        define('__DB_NAME__',__DEV_DB__);
}

// define the root directory
define('__ROOT__',$_SERVER['DOCUMENT_ROOT']);
foreach($ini['directories'] as $k => $v){
        if(!defined(strtoupper($k))){
		if($k != 'subdir'){
			//error_log("define('".strtoupper($k).",".__ROOT__.SUBDIR."/$v");
			define(strtoupper($k),__ROOT__.SUBDIR.'/'.$v);
		}else{
			//error_log("define('SUBDIR','$v')");
			define(strtoupper($k),$v);
		}
        }
}
// anything left over in the ini file
foreach($ini as $k => $v){
	processConfIniFile($v);
}


// process the rest of the INI file
foreach($ini as $k => $v){
	switch($v){
		case 'db' :
		case 'directories' :
		case 'system' : break;
		default :
			processConfIniFile($v);
		break;
	}
}

/**
*	Some checks on the definitions
*/
if(__DB_HOST__ == '' || !defined('__DB_HOST__')) { exit("Missing database host definition in conf.ini"); }
if(__DB_USER__ == '' || !defined('__DB_USER__')) { exit("Missing database USER definition in conf.ini"); }
if(__DB_PASS__ == '' || !defined('__DB_PASS__')) { exit("Missing database PASS definition in conf.ini"); }
if(__DB_TYPE__ == '' || !defined('__DB_TYPE__')) { exit("Missing database TYPE definition in conf.ini"); }
if(__DB_PORT__ == '' || !defined('__DB_PORT__')) { exit("Missing database PORT definition in conf.ini"); }
if(SCHEMA == ''      || !defined('SCHEMA'))      { exit("Missing SCHEMA definition in conf.ini"); }
//if(USER_SCHEMA == '' || !defined('__DB_HOST__')) { exit("Missing USER_SCHEMA definition in conf.ini"); }
if(__DB_NAME__ == '' || !defined('__DB_NAME__')) { exit("Missing database NAME definition in conf.ini"); }


/**
*	Default Directory Assignments
*/
// SUBDIR is used if you are building under a different domain name
// If SUBDIR is NOT set please ensure that the paramtere is
//	completely empty, DO NOT LEAVE the /
$SUBDIR="";

define('__HTTPS__',false);
if(__HTTPS__){
	define('__URL_BASE__',"http://".$_SERVER['SERVER_NAME']."$SUBDIR/");
	if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on'){
		header("Location: ".__URL_BASE__."/$SUBDIR/index.php");
		exit();
	}
}else{
	define('__URL_BASE__',"http://".$_SERVER['SERVER_NAME']."$SUBDIR/");
}

$__OS_TOOLS__ = array('imagic','quota');
?>
