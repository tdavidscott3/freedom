<?php
/**
 *	The purpose of this file is to auto-create the "database" connection using the configuration credentials located in the conf.php gile.
 *
 *	@author David Scott <tdavid.scott3@gmail.com>
 *	@version 1.2.2
 */



/**
*	database_class
*	@author David Scott <tdavid.scott3@gmail.com>
*/
class database_class{
	/**
	*	@var object $database declared as null
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public $database = null;

	/**
	*	@var string $db_type declared as null
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	private $db_type = null;
	
	/**
	*	@var object $debug declared as null
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	private $debug = null;
	
	/**
	*	@var string $conf declared as null
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	private $conf = null;
	

	/**
	*	__construct() - We create the database connection object based on the declarations definned in the conf.php file.
	*	@return object this->database
	*	@param string $conf you can specific a different conf file.
 	*	@author David Scott <tdavid.scott3@gmail.com>
	*	
	*/
	public function __construct($conf = null){
		if(!empty($conf)){ $this->conf = $conf; }else{ $this->conf = $_SERVER['DOCUMENT_ROOT'].'/library/conf.php'; }
		require_once($this->conf);

		//require_once($_SERVER['DOCUMENT_ROOT'].'/library/debug_class.php');
		require_once(__SYSTEM__.'/debug_class.php');
		$this->debug = new debug_class($this->conf);
		
		$this->db_type = __DB_TYPE__;
		switch($this->db_type){
			case 'mysql' :
			case 'my' :
				//require_once($_SERVER['DOCUMENT_ROOT'].'/library/mysql_class.php');
				require_once(__SYSTEM__.'/database_classes/'.__DB_TYPE__.'_class.php');
				require_once(__SYSTEM__.'/db_classes/mysql_class.php');
				$this->database = new mysql_class(__DB_HOST__,__DB_USER__,__DB_PASS__);
				//print('<h1 style="color: green;">'.SCHEMA.'</h1>');
				if(defined('SCHEMA') && SCHEMA != ''){
					exit("<b style=\"color: red;\">Can not use schema name '".SCHEMA."' when using mysql db structure.<br>Please either comment out the define('SCHEMA','".SCHEMA."'); or change it so that it appears as define('SCHEMA',''); in the conf.php file.<br></b>");
				}elseif(!defined('SCHEMA')){ define('SCHEMA',''); }
				
				//return $this->database;
			break;
			case 'pgsql' :
			case 'pg' :
				//require_once($_SERVER['DOCUMENT_ROOT'].'/library/pgsql_class.php');
				//require_once(__SYSTEM__.'/pgsql_class.php');
				require_once(__SYSTEM__.'/database_classes/'.__DB_TYPE__.'_class.php');
				$this->database = new pgsql_class(__DB_HOST__,__DB_NAME__,__DB_USER__,__DB_PASS__,__DB_PORT__);
				if(!defined('SCHEMA')){
					exit("<b style=\"color: red;\">Can not use PGSQL without a schema name.<br>Please uncomment and/or change the define('SCHEMA','".SCHEMA."'); line the conf.php file.<br></b>");
				}else{
					$schema = SCHEMA;
					//print("\$schema[".strlen($schema)."] = ".$schema[strlen($schema)-1]."<br>");
					if($schema[strlen($schema)-1] != '.'){
						//&& $schema[strlen($schema)] != '.'){
						exit("<b style=\"color: red;\">Can not use PGSQL without a trailing period (.) in the schema name.<br>Please add a period (.) to the end of '".SCHEMA."' so that it appears as '".SCHEMA.".' in the conf.php file.</b>");
					}
				}
				//return $this->database;
			break;
			case 'oracle' : 
				die($this->db_type.' is not yet implemented');
				require_once(__SYSTEM__.'/database_classes/'.__DB_TYPE__.'_class.php');
			break;
			case 'access' :
				die($this->db_type.' is not yet implemented');
				require_once(__SYSTEM__.'/database_classes/'.__DB_TYPE__.'_class.php');
			break;
			case 'mongo' :
				die($this->db_type.' is not yet implemented');
				require_once(__SYSTEM__.'/database_classes/'.__DB_TYPE__.'_class.php');
			break;
			case 'couch' :
				die($this->db_type.' is not yet implemented');
				require_once(__SYSTEM__.'/database_classes/'.__DB_TYPE__.'_class.php');
			break;
			default :
				exit("Unknown database type : ".__DB_TYPE__."<br>");
			break;
		}
		return $this->database;
	}
	
	/**
	*	fetch() - calls the correct fetch() method from the correct SQL_class file, which is dclared in the conf.php file.
	*	@param string $Q - the query to execute. (Should be a SELECT statment)
	*	@return array - array of an object
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function fetch($Q){
		return $this->database->fetch($Q);
	}
	
	/**
	*	query() - execute a sql query in the correct sql_class connector. This should be an INSERT or DELETE statement, that does not need a return value from the database.
	*	@return boolean true - always returns true
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function query($Q){
		// if(function_exists($this->db->query()){
			$this->database->query($Q);// or $this->database->error();
		// }
		return true;
	}
	
	/**
	*	returnFromFunction() - 
	*	@author unknown
	*	@param string $Q - sql query to execute
	*/
	public function returnFromFunction($Q = null){
		return $this->fetch($Q);	// we return the entire array
	}
	
	
	/**
	*	getDataTypeOfColumn() - get the data type of a column name from a passed in tablename - this is a pass through function
	*	@param string $tablename
	*	@param string $columname
	*	@return array $this->database->getDataType()
	*	@author unknown
	*/
	public function getDataTypeOfColumn($tablename, $columnname){
		return $this->database->getDataType($tablename,$columnname);
	}
	
	/**
	*	getAllTableNamesFromSchemaName() - get all the table names from the schema name
	*	@param string $schemaname default null
	*	@return array $this->database->getAllTableNamesFromSchemaName()
	*	@author unknown
	*/
	public function getAllTableNamesFromSchemaName($schemaname = null){
		return $this->database->getAllTableNamesFromSchemaName($schemaname);
	}
	
	/**
	*	getRowCountFromTableName() - get the count of rows of a given table name
	*	@param string $tablename default null
	*	@return array $this->database->getDataType()
	*	@author unknown
	*/
	public function getRowCountFromTableName($tablename = null){
		return $this->database->getRowCountFromTableName($dbname,$tablename);
	}
	
	/**
	*	getRowInformationFromTableName() - 
	*	@param string $tablename default null
	*	@return array $this->database->getRowInformationFromTableName()
	*	@author unknown
	*/
	public function getRowInformationFromTableName($tablename = null){
		return $this->database->getRowInformationFromTableName($tablename);
	}
	
	/**
	*	getTableCountFromSchemaName() - 
	*	@param string $schema default null
	*	@return array $this->database->getTableCountFromSchemaName()
	*	@author unknown
	*/
	public function getTableCountFromSchemaName($schema = null){
		return $this->database->getTableCountFromSchemaName(schemaname);
	}
	
	/**
	*	optimizeTable() - 
	*	@param string $dbname default null
	*	@param string $tablename default null
	*	@return array $this->database->optimizeTable()
	*	@author unknown
	*/
	public function optimizeTable($dbname = null, $tablename = null){
		return $this->database->optimizeTable($dbname,$tablename);
	}
	
	
	
}
?>
