<?php
/**
*	the datetime class is used for when retrieiving things out of the db.. or maybe you just
*	wanna be able to change the timezone very quickly.
*/



/**
*	datetime_class
*/
class datetime_class{

        public $timezone = 'America/New_York';  // this should match the currect EST/EDT timezone
	public $date_format = 'Y-m-d';
	public $time_format = 'H:i:s';

        public function __construct(){}

	/**
	*	get() - retrieve the val of a name item in this object
	*	@param string $item - the name of the item to return ($this->$item)
	*	@return variable - returns the value of $this->$item OR return false
	*/
        public function get($item){
                if(!empty($this->$item)){
                        return $this->$item;
                }

		return false;
        }

	/**
	*	set() - this will set a variable inside $this object
	*	@param string $item - the name of the item to use $this->$item
	*	@param variable $val - the value of the the item $this->$item = $val;
	*	@return variable $val
	*/
        public function set($item,$val){
                $this->$item = $val;
                return $this->$item;
        }


        /**
	*	time() - returns the time in the currently set $this->timezone
	*	@uses string $this->timezone
	*	@return string
	*/
	public function time(){
                date_default_timezone_set($this->timezone);
                return date($this->time_format);
        }

	/**
	*	date() - returns the date in the currently set $this->timezone
	*	@uses string $this->timezone
	*	@return string
	*/
        public function date($format = 'Y-m-d'){
                date_default_timezone_set($this->timezone);
                return date($this->date_format);
        }

	/**
	*	display() - this will display a time and/or date output depending on the format.
	*	the format can be passed in as a paramatere, OR will use $this->format as the output format.
	*	the timezone will need to be change prior to calling this function, as $this->timezone is used.
	*	@param string $format - OPTIONAL - use to change the outputing
	*	@param string $timestamp - OPTIOANL - used to change one timezone timestamp to another
	*	@param string $from - allows to use a different incoming time zzone to convert to the $this->timezone
	*	@return string
	*/
	public function display($format = null, $timestamp = null, $from = 'UTC'){
		date_default_timezone_set($from)
		$tz = strtotime(substr($timestamp,0,19));
                date_default_timezone_set($this->timezone);
		if($format == null){ $format = $this->format; }
		if($timestamp == null){
			return date($format);
		}else{
			return date($format, $tz);
		}
	}
}
?>
