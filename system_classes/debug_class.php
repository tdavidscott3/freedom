<?php
/**
*	@author David Scott <tdavid.scott3@gmail.com>
*	@version 1.0.1
*	debug_class.php
*/


/**
*	debug_class{}
*/
class debug_class{
	
	/**
	*	__construct() - this will call the session_start(), this allows to use this class / object without using the session_class
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function __construct(){
		@session_start();
	}

	/**
	*	display() - this will display in a nice little format the variable and/or title for debuging output.
	*	@param string $var default null
	*	@param string $title default null
	*	@return boolean - this will return true or false, false on error
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function display($var = null, $title = null){
		if($var == null || $var == ''){
			print("<b>Error occured attempting to display debug variable.</b><br>");
			return false;
		}
		
		?><pre style="border: 1px solid black; background-color: #CCCCCC;"><?php
		if($title != null){
			print("<h2>$title</h2>");
		}
		
		print_r($var);
		?></pre><?php
		return true;
	}
	
	/**
	*	displayGlobal() - display damn near everything you can think of.
	*	@todo since this is a extraction on the display() function, this should be calling $this->display() instead of what it is doing.....<b>DO IT OVER</b>
	*	@return boolean - always return true
	*	
	*/
	public function displayGlobal(){
		
		?><pre style="border: 1px solid black; background-color: #CCCCCC;"><?php
		$arrayGlobals = array(
			0 => array(0=>'$_SESSION', 1=> $_SESSION),
			1 =>  array(0=>'$_COOKIE', 1=> $_COOKIE),
			2 =>  array(0=>'$_GET', 1=> $_GET),
			3 =>  array(0=>'$_POST', 1=> $_POST),
			4 =>  array(0=>'$_SERVER', 1=> $_SERVER),
		);
		foreach($arrayGlobals as $k => $v){
			if(isset($v[1])){
				?>
					<pre style="border: 1px solid black; background-color: #CCCCCC;">
					<h2><?= $v[0]; ?></h2>
				<?php
				print_r($v[1]);
				?></pre><?php
			}
		}
		?></pre><?php
		return true;
	}
}
?>
