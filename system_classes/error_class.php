<?php
/**
*	error_class.php
*	@author David Scott <tdavid.scott3@gmail.com>
*/


/**
*	error_class{}
*/
class error_class{

	/**
	*	__construt()
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function __construct(){
	}
	
	/**
	*	display() - This should sisplay the error message to the onscreen
	*	@param string $var default null
	*	@param string $title default null
	*	@param boolean $stop default false
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function display($var = null, $title = null, $stop = false){
		if($var == null){
			print("<b>Error occured attempting to display debug variable</b><br>");
			return false;
		}
		
		
		?><pre style="border: 1px solid black; background-color: #CCCCCC;"><?php
		if($title != null){
			print("<h2>$title</h2>");
		}
		print_r($var);
		?></pre><?php
		
		if($stop){
			exit("Halt Recieved");
		}
	}

	/**
	*	handle() - this can (or should) be able to handle any and all errors that ALL code can throw at it
	*	the construct is the first argument is the function name, and the second is the array of paramters passed into the
	*	function (first argument)
	*	@param variable - no paramters are declared as the number of arguments that can be passed in are a variable item
	*	@return variable - the return depends on the functions that are passed in
	*	@example handle('function_name',array('user'=>$user));
	*	@todo - testing testing testing, not sure if this idea is even gonna work
	*/
	public function handle(){
		if(func_num_args()>0){
			$args = func_get_args;
			$function = $args[0];
			$string = '';
			foreach($args[1] as $k => $v){
				$$k => $v;	// 'user' == $user
				$string .= "${$k},";
			}
			eval($function($string));
		}else{
			return false;
		}
	}

	/**
	*	run() - some function that will handle the execute of other functions passed in
	*	@example run('print("hellow world"));
	*	@example run($this->log($userid,'entry','false'));
	*	@todo major testing
	*/
	public function run(){
		if(count(func_num_args()) > 0){
			foreach(func_get_args() as $k => $v){
			}
		}else{
			return false;
		}
	}

	/**
	*	warn() - this error pukes out anything and everything it can pertaining to an error
	*/
	public function warn(){
		return true;
	}

	/**
	*	warn() - this function takes a dump out and then halts. a fatal
	*/
	public function fatal(){
		exit();
		die();
		exit();
	}
}
?>
