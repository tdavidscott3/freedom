<?php
/**
*	We use this class to check for things we can and can not use in php
*	specifically this version (the version we are running) of php
*/
// load the config file
require_once($_SERVER['DOCUMENT_ROOT'].'/library/conf.php'); 


function checkVersion(){
	if(__USE_CLASSES && version_compare(phpversion(),'5.3.0','<')){
		exit('Your current version of PHP, '.phpversion().' does not support classes, you might want to upgrade to a PHP version higher then 5.3.0');
	}

	if(__USE_TRAITS__ && version_compare(phpversion(),'5.4.0','<')){
		exit('The __USE_TRAITS__ flag is set to true. This application is using traits and can not be used in PHP version lower then 5.4.0, your current version is '.phpversion().'. Sorry');
	}
}

/**
*	We do all the magic here... so let's test
*/
checkVersion();
?>
