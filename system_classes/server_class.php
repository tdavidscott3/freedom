<?php
/**
*	server_class.php - all interactions on the server level of things.
*	@author David Scott <tdavid.scott3@gmail.com>
*	@version 1.4.2
*/


/**
*	server_class{}
*/
class server_class{

	/** @var string $myIp default null */
	public $myIP = null;
	/** @var string $remoteIP default null */
	public $remoteIP = null;
	/** @var $tools default null */
	public $tools = null;
	/** @var string $conf default null */
	public $conf = null;

	/**
	*	__construct() - assigns the $this->remoteIP and the $this->myIP
	*	@var string $this->myIP - server's IP
	*	@var string $this->remoteIP - users IP
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function __construct($conf = null){
		require_once($_SERVER['DOCUMENT_ROOT'].'/library/conf.php');
		/*if(isset($conf) && $conf != null){
			$this->conf = $conf;
		}
		if(!file_exists($this->conf)){
			exit("Unable to locate ".$this->conf);
		}
		require_once($this->conf);
		*/
		$this->myIP = $_SERVER['SERVER_ADDR'];
		$this->remoteIP = $_SERVER['REMOTE_ADDR'];
	}
	
	/**
	*	returnRemoteIP() - returns the users IP as stored in $this->remoteIP
	*	@return string $this->remoteIP
	*/
	public function returnRemoteIP(){
		//return $_SERVER['REMOTE_ADDR'];
		return $this->remoteIP;
	}
	
	/**
	*	checkTools() - check the tool array definned in the conf.php file.... I think
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function checkTools(){
		// we are chekcing for tools needed
		// if(defined(__OS_TOOLS__)){
			// $this->tools = __OS_TOOLS__;
		// }
		if($this->tools == null || !is_array($this->tools)){
			exit("You will need to set the tools array located in the ".basename(__FILE__).".<br>In order to do so, please use the following format.<br>
			<br><pre style=\"border: solid black 1px; background-color: #FEFFE3;\">
require_once(".basename(__FILE__).");
\$server = new server_class();
\$server->tools = array('glibc','imagic','some_other_tool');
			</pre>
			Thank you.
			");
		}
		
		foreach($this->tools as $k => $v){
			$located = shell_exec("whereis $v | cut -f2 -d\":\" | tr -d \"\\n\"");
			if($located == ''){
				die("<h1>can not locate os_level command '$v'</h1>");
			}
			// print(shell_exec("whereis $v | cut -f2 -d\" \" | tr -d \"\\n\""));
			$this->myTools[$v] = shell_exec("whereis $v | cut -f2 -d\" \" | tr -d \"\\n\"");
		}
	}
	
	/**
	*	exec() - execute a command as a user on the shell
	*	@param string $user default-less
	*	@param string $command default-less
	*	@uses log_class::logThis()
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function exec($user, $command){
		// a wrapper!
		// print("<h2>\$command = '$command'</h2>");
		if(preg_match('/^rm/i',$command)){
			die("Can not execute a 'rm' command this way");
		}
		// we log all these commands!!!
		$output = '['.date('r').'] : ('.$user.') -> '.$command.'';
		// print($output);
		$this->logThis($output."\n");
		return shell_exec($command);
	}
	
	/**
	*	logThis() - insert the passed in string into a php_shell.log file
	*	@param string $string default-less
	*	@author David Scott <tdavid.scott3@gmail.com>
	*	
	*/
	public function logThis($string){
		$log = __ROOT__.'/logs/php_shell.log';
		if(!is_dir(__ROOT__.'/logs')){ mkdir(__ROOT__.'/logs'); }
		if(!file_exists($log)){
			touch($log);
		}
		if($string[strlen($string)-1] != "\n"){ $string .= "\n"; }
		$handle = fopen($log,'a');
		fwrite($handle,$string);
		fclose($handle);
		/* $preLog = file_get_contents($log);
		$newInput = $preLog . "$string\n";
		// $preLog .= "$string\n";
		file_put_contents($log,$newInput); */
	}
	
	/**
	*	getFullQuotaOutput() - gets a FS quota based on the passed in owner, due to logging, the user is who is executing the command.
	*	@param string $user default-less
	*	@param string $owner default-less
	*	@return string
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function getFullQuotaOuput($user, $owner){
		return $this->exec($user,"sudo quota -u $owner");
		
	}
	
	/**
	*	getQuota() - get the quota of the group from the passed in owner, $use is the user who is performing the search on the owner
	*	@param string $owner default-less
	*	@param string $user default-less
	*	@uses log_class::logThis
	*	@return string
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function getQuota($user, $owner){
		return $this->exec($user, "sudo quota -g $owner | awk '{print $4}' | tail -n 1");
		// settype($R,'integer');
		// print("<h1>$R</h1>");
		// return $R;
	}
	
	/**
	*	getQuotaUsage() - get how much of the quota is used, this function 
	*	@uses $this::logThis()
	*	@param string $user default-less
	*	@param string $owner default-less
	*	@author David Scott <tdavid.scott3@gmail.com>
	*	@return string
	*/
	public function getQuotaUsage($user, $owner){
		return $this->exec($user,"sudo quota -g $owner | awk '{print $2}' | tail -n 1");
		// settype($USED,'integer');
		// settype($R,'integer');
		// return $R;
	}
	
}
?>
