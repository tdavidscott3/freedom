<?php
/**
*	session_class.php
*	This file handles <b>ALL</b> interactions with the $_SESSION array/object
*	@author David Scott <tdavid.scott3@gmail.com>
*/

/**
*	The session_class()
*/
class session_class{

	public $debug = null;
	//public $error = null;
	public $conf = null;
	
	/**
	*	__construct()
	*	@param $conf default is null - this is an allowed to bypass the default conf.php file.
	*	@param $redir default is null - $redir is used for when a browser has exceeded the time limit and then is redirected
	*	@param $force default is false - used to bypass the redirection based on the session time limit. usefull when normal pages, that users are not required to refresh the pages. for example, when you have someone that is logged in.
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function __construct($conf = null, $redir = null, $force = false){
		@session_start();
		$this->conf = $_SERVER['DOCUMENT_ROOT'].'/library/conf.php';
		if(!empty($conf)){ $this->conf = $conf; }
		if(!file_exists($this->conf)){
			exit('Unable to load '.$this->conf);
		}else{
			require_once($this->conf);
		}
		//$this->debug = $this->loadThisClass('library/debug_class.php','debug_class',__FILE__,__LINE__);
		
		if(!$this->checkSession() && $force){
			$this->close($redir);
		}
		require_once(__SYSTEM__.'/debug_class.php');
		$this->debug =  new debug_class();
		
		require_once(__SYSTEM__.'/common_class.php');
		$this->common =  new common_class();
	}

	/**
	*	checkSession()
	*	@param $redir default false
	*	@param $location default null
	*	@param $tl default 3600 - the time to live, session expires
	*	@return boolen_OR_array - see to do item
	*	@todo Fix the return, it currently returns boolean OR array, it should return an array only.
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function checkSession($redir = false, $location = null, $tl = 3600){
		if(!isset($_SESSION['start_time'])){
			$_SESSION = array(
				'start_time'	=> date("U"),
				'end_time'		=> date("U")+$tl,
				'right_now'		=> date("U"),
				'email'			=> 'null',
			);
		}else{
			settype($_SESSION['end_time'],'integer');
			settype($_SESSION['right_now'],'integer');
			$right_now = date("U");
			settype($right_now,'integer');
			
			if($_SESSION['end_time'] < $right_now){
				return array(0=>'error',1=>__EXCEED_SESSION_TIME__);
			}
			$_SESSION['right_now'] = date("U");
			$_SESSION['end_time'] = date("U")+$tl;
		}
		return true;
	}

	/**
	*	loadFile() check and require_once a file passed in. $FILE and $LINE are used for backwards debugging. So you can tell which file and what line of that file called the loadFile() function.
	*	@param string $filename default null
	*	@param string $FILE default __FILE__
	*	@param integer $LINE default __LINE__
	*	@param boolean $disError default true - to display the error or not
	*	@return boolean always true
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function loadFile($filename = null, $FILE = __FILE__, $LINE = __LINE__, $disError = true){
		/*
			We are allowed to skip this file if it doesn't exist.
		*/
		if($filename == null){
			if($disError){
				print("<b>Missing FILENAME for include_once() from $FILE on #$LINE</b><br>");
			}
		}else{
			// we sqaush the warning if the file doesn't exists.
			if(file_exists($filename)){
				include_once($filename);
			}else{
				if($disError){
					print("<b>File Not Found : <b style=\"color: red;\">$filename</b>> : unable to include_once() file listed in $FILE on #$LINE</b><br>");
				}
			}
		}
		return true;
	}

	/**
	*	loadClass() - This method will check, require_once, create an object and return said object
	*	@param string $filename default null
	*	@param string $classname default null
	*	@param string $FILE defailt __FILE__
	*	@param integer $LINE default __LINE__
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function loadClass($filename = null, $classname = null, $FILE = __FILE__, $LINE = __LINE__, $disError = true){
		if($filename == null) {
			if($disError){
				print("<b>Missing filename for loadThisClass() from $FILE on #$LINE</b><br>");
			}
			return array(0=>'error',1=>"No Filename received FILE :: $FILE on LINE # $LINE");
		}
		if($classname == null){
			if($disError){
				print("<b>Missing classname for loadThisClass() from $FILE on #$LINE</b><br>");
			}
			return array(0=>'error',1=>"No Filename received FILE :: $FILE on LINE # $LINE");
		}
		
		// we got here so we can do the checks now
		if(!file_exists($filename)){
			if($disError){
				print("<b>Missing $filename $FILE on #$LINE :: Cannot load class '$classname'</b><br>");
			}
			return array(0=>'error',1=>"Missing $filename file included from $FILE on # $LINE");
		}else{
			require_once($filename);	// always based this off the ROOT of the site.
			$myTempObject = new $classname($this->conf);
			return $myTempObject;
		}
	}

	/**
	*	checkAndRedir() - This method will check a $_SESSION[_key_] and redirect IF it is empty, null, 'null', or is not set
	*	@param string $key default null - _SESSION[<b>$key</b>]
	*	@param variable value default null - the value of the $key to be checked against
	*	@param string $redir default null - redirect to this location if the $key check fails
	*	@return boolean true - returns true if check passes, otherwise, redirection occurs
	*/
	public function checkAndRedir($key=null, $value = null, $redir = null){
		//if($_SESSION[$key] == null || empty($_SESSION[$key]) || $_SESSION[$key] == '' || $_SESSION[$key] == 'null'){
		if(!isset($_SESSION[$key]) || $_SESSION[$key] != $value){
			header("Location: $redir");
			return false;
		}
		return true;
	}
	
	/**
	*	close() - this method will perform a session_close(), session_destroy and redirect
	*	@param string $redir - redirect to this value
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function close($redir = "/"){
		$_SESSION=array();
		unset($_SESSION);
		session_destroy();
		header("Location: $redir");
		exit();
	}

	/**
	*	redirect() - redirect to the parameter passed
	*	@param string $redir - location to redirect to
	*	@param string $error default 'no redirection'
	*/
	public function redirect($redir = null, $error = "no redirection"){
		if(empty($redir) || $redir == null){
			print("No redirection location specified.");
			return false;
		}
		//$this->writeClose();
		header("Location: $redir") or exit("<font color=\"red\">$error</font><br>".__URL_BASE__);
	}
	
	/**
	*	destroyAndRedirect() 
	*	@param string $redir default '/' - redirect to this value
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function destroyAndRedirect($redir = '/'){
		$this->destroy();
		$this->redirect($redir);
		//header("Location: $redir") or exit("WHY DIDN'T WE REDIRECT?<br>".__URL_ROOT__);
		exit();
	}

	/**
	*	destroy() - This functioin clears the _SESSION and the calls the session_destroy() function
	*	@return boolean false - this will always return false
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function destroy(){
		$_SESSION=array();
		unset($_SESSION);
		session_destroy();
		$this->checkSession();
		return false;
	}

	/**
	*	trueDestroy() - This seems to be dealing with session and cookie manipulations.
	*	@return boolean false - this method ALWAYS return false.
	*	@author unknown
	*/
	public function trueDestroy(){
        	$CookieInfo = session_get_cookie_params();
               	if ( (empty($CookieInfo['domain'])) && (empty($CookieInfo['secure'])) ) {
               		setcookie(session_name(), '', time()-3600, $CookieInfo['path']);
               	} elseif (empty($CookieInfo['secure'])) {
               		setcookie(session_name(), '', time()-3600, $CookieInfo['path'], $CookieInfo['domain']);
               	} else {
               		setcookie(session_name(), '', time()-3600, $CookieInfo['path'], $CookieInfo['domain'], $CookieInfo['secure']);
               	}
		$_SESSION=array();
		unset($_SESSION);
		session_destroy();
		$this->checkSession();
		return false;
	}
	
	/**
	*	Sourced from PHPhub - gets session cookie info and destroys cookie. session_destroy(); does not automatically
	*	destory the session cookie. The second function does not destroy the session, only the cookie. UPDATE 
	*	Only cookie destroy function remains, trueDestroy contains functionality of completely destroying the session+cookie. 
	*/
	public function destroySessionCookieOnly($Ctime = null){
		$CookieInfo = session_get_cookie_params();
		if ( (empty($CookieInfo['domain'])) && (empty($CookieInfo['secure'])) ) {
			setcookie(session_name(), '', time()-3600, $CookieInfo['path']);
		} elseif (empty($CookieInfo['secure'])) {
			setcookie(session_name(), '', time()-3600, $CookieInfo['path'], $CookieInfo['domain']);
		}else {
			setcookie(session_name(), '', time()-3600, $CookieInfo['path'], $CookieInfo['domain'], $CookieInfo['secure']);
		}
	}
	
	/**
	*	writeClose() - This will call a session_write_close()
	*	@return boolean false
	*	@author David Scott <tdavid.scott3@gmail.com>
	*/
	public function writeClose(){
		session_write_close();
		return true;
	}

	/**
	*	getSessionStatus() - check for _SESSION['status'] looking for 'disabled'
	*	@return boolean true - always return true
	*	@author Unknown
	*/
	public function getSessionStatus(){
		// this is our loged in value
		if($_SESSION['status'] == 'disabled'){ return false; }
		return true;
	}
	
	/**
	*	redirectViaJavascript() - we force the browser to use JavaScript to redirect to a new page, this is useful, if header() function in php can'ty redirect due to output already on the page. this is a location based off URL_ROOT
	*	@param string $file - default null - redirect using javascript to the __UIRL_ROOT_ + $file
	*	@author unknown
	*/
	public function redirectViaJavascript($file = null){
        	$new_location = __URL_ROOT__."/$file";
        	print("<script type='text/javascript'>window.location='$new_location';</script>");
		exit();
	}
	
	/**
	*	set() - Sets a _SESSION[$key] = $val and returns the $val
	*	@param string $key default null
	*	@param variable $val default null
	*	@return variable $val default null
	*
	*/
	public function set($key = null, $val = null){
		if($key != null){ $_SESSION[$key] = $val; }
		return $val;
	}

	/**
	*	get() - retrieves and returns a _SESSION[<b>$key</b>] lookup
	*	@param string $key default is null
	*	@return string - returns the value of _SESSION[$key] or 'null'
	*/
	public function get($key = null){
		if(isset($_SESSION[$key]) && $_SESSION[$key] != null){
			return $_SESSION[$key];
		}else{
			return 'unset';	// not sure oabout this one.
		}
	}
	
	/**
	*	openDiv() - 
	*	@param string $id - 
	*	@return boolean - true -- always returning true
	*	@todo This does not need to be in the session_class() - <b>MOVE THIS</b>
	*/
	public function openDiv($id){
		?>
		
		<rel link="stylesheet" type="text/css" href="<?= __URL_ROOT__."/includes/css/$id.css"; ?>"/>
		<div id="<?= $id; ?>">
		
		<?php
		return true;
	}

	/**
	*	closeDiv() - 
	*	@param string $id - 
	*	@return boolean true - always returns true
	*	@todo This does not need to be in the session_class() - <b>MOVE THIS</b>
	*/
	public function closeDiv($id){
		?>
		
		</div><!-- end of div <?= $id; ?> -->
		
		<?php
		return true;
	}

	/**
	*	dump_input()
	*	@param boolean $return default false
	*	@author Josh Chick <joshchick@gmail.com>
	*	@todo - need to rewrite this using the debugging->display() object method.
	*/
    
    public function dump_input($return = false) {
        $dump_stuff = '';
        if ($_POST || $_GET || $_COOKIES || $_SESSION) {
            $dump_stuff .= "<pre>";
            foreach ($_POST as $key => $value) {
                $dump_stuff .= "P: $key = $value\n";
            }
            foreach ($_GET as $key => $value) {
                $dump_stuff .= "G: $key = $value\n";
            }
            foreach ($_COOKIE as $key => $value) {
                $dump_stuff .= "C: $key = $value\n";
            }
            foreach ($_SESSION as $key => $value) {
                $dump_stuff .= "S: $key = $value\n";
            }
            $dump_stuff .= "</pre>\n";
        }
        if ($return) {
            return $dump_stuff;
        }
        echo $dump_stuff;
    }

	/**
	*	checkThis() - this will return true if the $_SESSION[$key] is set and NOT a blank string
	*	@param string $key
	*	@return boolean
	*/
	public function checkThis($key = ''){
		if(isset($_SESSION[$key]) && $_SESSION[$key] != ''){
			return true;
		}else{
			return false;
		}
	}
}

?>
