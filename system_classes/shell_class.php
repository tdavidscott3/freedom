<?php
/**
*       shell_class - a class to perform all our interactions on a shell level.
*       this also includes creating and writing to a log_file on the system of EVERYTHING we do inside this class.
*       @author David Scott <tdavid.scott3@gmail.com>
*/

/**
*       The shell_class()
*/
class shell_class{
	public $conf = null;
	public $error = null;
	public $logFile = null;

	public function __construct($conf = null){
		if($conf == null){
			$this->conf = $_SERVER['DOCUMENT_ROOT'].'/conf.php';
		}else{
			$this->conf = $conf;
		}

		if($this->logFile == null){
			$this->logFile = '/var/log/apache2/shell_class.log';
		}

		require_once($this->conf);
		require_once(__SYSTEM__.'/error_class.php');
		$error = new error_class();
	}


	public function exec($command = null){
		if($command == null){ return 'No command to process'; }
		/*
			some checks.
		*/
		$cmdArray = explode(" ",$command);
		foreach($cmdArray as $k => $v){
			switch($v){
				case 'rm' :
					$this->logWrite("$command contains 'rm'");
					return "command '$command' contains 'rm' :: can not execute";
				break;
			}
		}

		$output = shell_exec($command);
		$this->logWrite("exec($command) == $output");
		return $output;
	}


	/*
	*       #################################################################
	*/
	private function checkLogFile(){
		if(!file_exists($this->logFile)){
			file_put_contents($this->logFile,"[".date('Y-m-d H:i:s').'] initial creation of log file') or die('unable to create .shell_class.log file');
		}
	}
	private function logWrite($entry){
		$this->checkLogFile();
		$fh = fopen($this->logFile,'a');
		fwrite($fh, "[".date("Y-m-d H:i:s")."] $entry");
		fclose($fh);
	}
}
?>
